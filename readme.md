Materials per al mòdul *M7 - Serveix de xarxa* del cicle de *Sistemes microinformàtics i xarxes*
=====

Índex
-----

### UF 1: configuració de la xarxa (DNS i DHCP)

1. Preparació entorn
  - [Virtualbox](UF1/preparacio_entorn/virtualbox.adoc)
  - [Preparació entorn](UF1/preparacio_entorn/preparacio_entorn.adoc)
2. DHCP
  - [Teoria](UF1/dhcp)
  - [Pràctica](UF1/dhcp/practica_dhcp.adoc)
2. DNS
  - [Teoria](UF1/dns)
  - [Pràctica](UF1/dns/practica_dns.adoc)

### UF 2: correu electrònic i transmissió d’arxius

1. Correu electrònic
2. FTP

### UF 3: servidor web i servidor intermediari (proxy)

1. Servidor HTTP
2. Servidor proxy

### UF 4: accés a sistemes remots

1. Accés a sistemes remots
